package petshop.servicemanipulation.controllers;

import lombok.AllArgsConstructor;
import lombok.Builder;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import petshop.servicemanipulation.dtos.AnimalServiceDto;
import petshop.servicemanipulation.models.AnimalServiceModel;
import petshop.servicemanipulation.services.AnimalServiceService;

import javax.validation.Valid;
import java.util.List;
import java.util.Optional;

@Builder
@AllArgsConstructor
@RestController
@CrossOrigin(origins = "*", maxAge = 3600)
@RequestMapping("/animal-service")
public class AnimalServiceController {

    final AnimalServiceService animalServiceService;

    @PostMapping
    public ResponseEntity<Object> saveAnimalService(@RequestBody @Valid AnimalServiceDto animalServiceDto){
        if (animalServiceService.existsAnimalService(animalServiceDto)){
            return ResponseEntity.status(HttpStatus.CONFLICT).body("Conflict: This service already exists");
        }

        if (animalServiceService.existsAnimal(animalServiceDto.getAnimalId()) == null){
            return ResponseEntity.status(HttpStatus.NOT_FOUND).body("This animal not exists.");
        }

        if (animalServiceService.existsService(animalServiceDto.getServiceId()) == null){
            return ResponseEntity.status(HttpStatus.NOT_FOUND).body("This service not exists.");
        }

        if (animalServiceService.existsEmployee(animalServiceDto.getEmployeeId()) == null){
            return ResponseEntity.status(HttpStatus.NOT_FOUND).body("This employee not exists.");
        }

        return ResponseEntity.status(HttpStatus.CREATED).body(animalServiceService.save(animalServiceDto));
    }

    @GetMapping
    public ResponseEntity<List<AnimalServiceModel>> findAllAnimalServices(){
        return ResponseEntity.status(HttpStatus.OK).body(animalServiceService.findAll());
    }

    @GetMapping("/{id}")
    public ResponseEntity<Object> findOneAnimalService(@PathVariable(value = "id") Long id){
        Optional<AnimalServiceModel> animalServiceModelOptional = animalServiceService.findById(id);

        if (!animalServiceModelOptional.isPresent()) {
            return ResponseEntity.status(HttpStatus.NOT_FOUND).body("Animal Service not found");
        }

        return ResponseEntity.status(HttpStatus.FOUND).body(animalServiceModelOptional.get());
    }

    @PutMapping("/{id}")
    public ResponseEntity<Object> updateAnimalService(@PathVariable(value = "id") Long id, @RequestBody @Valid AnimalServiceDto animalServiceDto){
        Optional<AnimalServiceModel> animalServiceModelOptional = animalServiceService.findById(id);

        if (!animalServiceModelOptional.isPresent()) {
            return ResponseEntity.status(HttpStatus.NOT_FOUND).body("Animal Service not found");
        }

        if (animalServiceService.existsAnimalService(animalServiceDto)){
            return ResponseEntity.status(HttpStatus.CONFLICT).body("Conflict: This service already exists");
        }

        if (animalServiceService.existsAnimal(animalServiceDto.getAnimalId()) == null){
            return ResponseEntity.status(HttpStatus.NOT_FOUND).body("This animal not exists.");
        }

        if (animalServiceService.existsService(animalServiceDto.getServiceId()) == null){
            return ResponseEntity.status(HttpStatus.NOT_FOUND).body("This service not exists.");
        }

        if (animalServiceService.existsEmployee(animalServiceDto.getEmployeeId()) == null){
            return ResponseEntity.status(HttpStatus.NOT_FOUND).body("This employee not exists.");
        }

        return ResponseEntity.status(HttpStatus.OK).body(animalServiceService.update(id, animalServiceDto));
    }

    @DeleteMapping("{id}")
    public ResponseEntity<String> deleteAnimalService(@PathVariable(value = "id") Long id){
        Optional<AnimalServiceModel> animalServiceModelOptional = animalServiceService.findById(id);

        if (!animalServiceModelOptional.isPresent()) {
            return ResponseEntity.status(HttpStatus.NOT_FOUND).body("Animal Service not found");
        }

        animalServiceService.delete(animalServiceModelOptional.get());
        return ResponseEntity.status(HttpStatus.OK).body("Animal service deleted successfully");
    }

}
