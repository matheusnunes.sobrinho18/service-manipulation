package petshop.servicemanipulation.repositories;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import petshop.servicemanipulation.models.AnimalServiceModel;

import java.util.Date;

@Repository
public interface AnimalServiceRepository extends JpaRepository<AnimalServiceModel, Long> {

    boolean existsByAnimalIdAndServiceIdAndEmployeeIdAndServiceDate(Long animalId, Long serviceId, Long employeeId, Date serviceDate);

}
