package petshop.servicemanipulation.enums;

public enum AnimalSize {
    Small,
    Medium,
    Large
}
