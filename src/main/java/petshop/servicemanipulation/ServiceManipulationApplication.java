package petshop.servicemanipulation;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

@SpringBootApplication
@RestController
public class ServiceManipulationApplication {

	public static void main(String[] args) {
		SpringApplication.run(ServiceManipulationApplication.class, args);
	}

	@GetMapping("/")
	public String index(){
		return "Felipe Idiota!";
	}

}
