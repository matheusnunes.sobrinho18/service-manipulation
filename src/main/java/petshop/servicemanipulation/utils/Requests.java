package petshop.servicemanipulation.utils;

import org.springframework.http.HttpStatus;
import org.springframework.web.client.HttpClientErrorException;
import org.springframework.web.client.RestTemplate;
import petshop.servicemanipulation.dtos.AnimalDto;
import petshop.servicemanipulation.dtos.EmployeeDto;
import petshop.servicemanipulation.dtos.ServiceDto;

public class Requests {

    private static RestTemplate restTemplate = new RestTemplate();
    private static String uriAnimalManipulation = "http://localhost:8080";

    public static ServiceDto findServiceById(Long id){
        String uri = uriAnimalManipulation + "/service/" + id;

        ServiceDto serviceDto;
        try {
            serviceDto = restTemplate.getForObject(uri, ServiceDto.class);
        } catch (HttpClientErrorException hce){
            throw new HttpClientErrorException(HttpStatus.NOT_FOUND, "Service not found");
        }
        return serviceDto;
    }

    public static AnimalDto findAnimalById(Long id){
        String uri = uriAnimalManipulation + "/animal/" + id;

        AnimalDto animalDto;
        try {
            animalDto = restTemplate.getForObject(uri, AnimalDto.class);
        } catch (HttpClientErrorException hce){
            throw new HttpClientErrorException(HttpStatus.NOT_FOUND, "Animal not found");
        }

        return animalDto;
    }

    public static EmployeeDto findEmployeeById(Long id) {
        String uri = uriAnimalManipulation + "/employee/" + id;

        EmployeeDto employeeDto;
        try {
            employeeDto = restTemplate.getForObject(uri, EmployeeDto.class);
        } catch (HttpClientErrorException hce){
            throw new HttpClientErrorException(HttpStatus.NOT_FOUND, "Employee not found");
        }

        return employeeDto;
    }
}
