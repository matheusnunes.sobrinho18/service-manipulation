package petshop.servicemanipulation.dtos;

import lombok.*;
import petshop.servicemanipulation.enums.EmployeeFunction;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;

@Getter
@Setter
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class EmployeeDto {

    @NotNull
    private Long id;

    @NotBlank
    private String name;

    @NotBlank
    private EmployeeFunction function;

}
