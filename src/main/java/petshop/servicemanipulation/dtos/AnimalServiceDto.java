package petshop.servicemanipulation.dtos;

import lombok.*;

import javax.validation.constraints.NotNull;
import java.util.Date;

@Builder
@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter
public class AnimalServiceDto {

    @NotNull
    private Long animalId;

    @NotNull
    private Long serviceId;

    @NotNull
    private Long employeeId;

    @NotNull
    private Date serviceDate;

}
