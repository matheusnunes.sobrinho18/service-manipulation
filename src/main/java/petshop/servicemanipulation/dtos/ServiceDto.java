package petshop.servicemanipulation.dtos;

import lombok.*;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;

@Getter
@Setter
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class ServiceDto {

    @NotNull
    private Long id;

    @NotBlank
    private String name;

    @NotNull
    private Double price;
}
