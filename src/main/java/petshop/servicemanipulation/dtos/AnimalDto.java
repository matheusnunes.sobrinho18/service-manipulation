package petshop.servicemanipulation.dtos;

import lombok.*;
import petshop.servicemanipulation.enums.AnimalSize;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;

@Getter
@Setter
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class AnimalDto {

    @NotNull
    private Long id;

    @NotBlank
    private String name;

    @NotBlank
    private String specie;

    @NotNull
    private AnimalSize animalSize;

    @NotNull
    private Long clientId;
}
