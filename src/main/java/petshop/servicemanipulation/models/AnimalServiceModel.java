package petshop.servicemanipulation.models;

import lombok.*;
import petshop.servicemanipulation.enums.AnimalSize;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Date;

@Entity
@Table(name = "TB_ANIMAL_SERVICE")
@Getter
@Setter
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class AnimalServiceModel implements Serializable {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(nullable = false)
    private Long animalId;

    @Column(nullable = false)
    private Long serviceId;

    @Column(nullable = false)
    private Long employeeId;

    @Column(nullable = false)
    private Date serviceDate;

    @Column(nullable = false)
    private Double finalPrice;

    public void setFinalPrice(AnimalSize animalSize, Double servicePrice) {
        switch (animalSize){
            case Small:
                this.finalPrice = servicePrice * 1.0;
            break;

            case Medium:
                this.finalPrice = servicePrice * 1.5;
            break;

            case Large:
                this.finalPrice = servicePrice * 2.0;
            break;
        }
    }
}
