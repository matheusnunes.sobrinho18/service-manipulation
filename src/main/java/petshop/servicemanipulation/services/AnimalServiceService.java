package petshop.servicemanipulation.services;

import lombok.AllArgsConstructor;
import lombok.Builder;
import org.springframework.beans.BeanUtils;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.client.HttpClientErrorException;
import petshop.servicemanipulation.dtos.AnimalDto;
import petshop.servicemanipulation.dtos.AnimalServiceDto;
import petshop.servicemanipulation.dtos.EmployeeDto;
import petshop.servicemanipulation.dtos.ServiceDto;
import petshop.servicemanipulation.models.AnimalServiceModel;
import petshop.servicemanipulation.repositories.AnimalServiceRepository;
import petshop.servicemanipulation.utils.Requests;

import java.util.Date;
import java.util.List;
import java.util.Optional;

@Service
@Builder
@AllArgsConstructor
public class AnimalServiceService {

    final AnimalServiceRepository animalServiceRepository;

    @Transactional
    public AnimalServiceModel save(AnimalServiceDto animalServiceDto) {
        AnimalServiceModel animalServiceModel = new AnimalServiceModel();
        BeanUtils.copyProperties(animalServiceDto, animalServiceModel);

        ServiceDto serviceDto = Requests.findServiceById(animalServiceDto.getServiceId());
        AnimalDto animalDto = Requests.findAnimalById(animalServiceDto.getAnimalId());

        animalServiceModel.setFinalPrice(animalDto.getAnimalSize(), serviceDto.getPrice());

        return animalServiceRepository.save(animalServiceModel);
    }

    public boolean existsAnimalService(AnimalServiceDto animalServiceDto){
        Long animalId = animalServiceDto.getAnimalId();
        Long serviceId = animalServiceDto.getServiceId();
        Long employeeId = animalServiceDto.getEmployeeId();
        Date serviceDate = animalServiceDto.getServiceDate();
        return animalServiceRepository.existsByAnimalIdAndServiceIdAndEmployeeIdAndServiceDate(animalId, serviceId, employeeId, serviceDate);
    }

    public AnimalDto existsAnimal(Long id){
        AnimalDto animalDto;
        try {
            animalDto = Requests.findAnimalById(id);
        } catch (HttpClientErrorException hce) {
            animalDto = null;
        }
        return animalDto;
    }

    public ServiceDto existsService(Long id){
        ServiceDto serviceDto;
        try {
            serviceDto = Requests.findServiceById(id);
        } catch (HttpClientErrorException hce) {
            serviceDto = null;
        }
        return serviceDto;
    }

    public EmployeeDto existsEmployee(Long id) {
        EmployeeDto employeeDto;
        try {
            employeeDto = Requests.findEmployeeById(id);
        } catch (HttpClientErrorException hce) {
            employeeDto = null;
        }
        return employeeDto;
    }

    public List<AnimalServiceModel> findAll() {
        return animalServiceRepository.findAll();
    }

    public Optional<AnimalServiceModel> findById(Long id) {
        return animalServiceRepository.findById(id);
    }

    public AnimalServiceModel update(Long id, AnimalServiceDto animalServiceDto) {
        AnimalServiceModel animalServiceModel = new AnimalServiceModel();
        BeanUtils.copyProperties(animalServiceDto, animalServiceModel);

        ServiceDto serviceDto = Requests.findServiceById(animalServiceDto.getServiceId());
        AnimalDto animalDto = Requests.findAnimalById(animalServiceDto.getAnimalId());

        animalServiceModel.setFinalPrice(animalDto.getAnimalSize(), serviceDto.getPrice());
        animalServiceModel.setId(id);

        return animalServiceRepository.save(animalServiceModel);
    }

    @Transactional
    public void delete(AnimalServiceModel animalServiceModel){
        animalServiceRepository.delete(animalServiceModel);
    }

}
